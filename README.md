# Streams in Java 8+

## Create a stream

### From collection

```java
Collection.stream()
Collection.parallelStream()
```

### Stream.of

```java
Stream.of(2, 3, 5, 7).forEach(System.out::println);
```


### Empty stream

```java
Stream.empty()
```


### Primitive type streams

```java
LongStream.range(0, 10).forEach(System.out::println);
IntStream.rangeClosed(2, num)
```

### Characters and lines in a `String`

```java
String str = "";
IntStream charStream = str.chars();
Stream<String> lineStream = str.lines();
```

### Lines from a file

```java
Stream<String> lineStream = Files.lines(Paths.get(filename));
```

### From array

```java
Arrays.stream(new int[]{1, 2, 3}).forEach(System.out::print);
```

### Iterate

```java
        IntStream.iterate(0, n -> n < 10, n -> n + 1)
                .forEach(System.out::print);

        System.out.println();

        IntStream.iterate(0, n -> n + 1)
                .takeWhile(n -> n < 10) // Can be replaced with three-arg 'iterate()', see above.
                .forEach(System.out::print);

        System.out.println();
```


### Generate

```java

        Stream.generate(Math::random)
                .limit(5)
                .forEach(System.out::println);
```


### Complex generation

#### Fibonacci numbers

```java
        Stream.iterate(new int[]{0, 1}, p -> new int[] { p[1], p[0] + p[1] })
                .map(p -> p[0])
                .limit(10)
                .forEach(System.out::println);
```

#### Prime numbers

```java
    static boolean isPrime(long x)
    {
        return x >= 2 && LongStream.range(2, x).map(y -> x % y).noneMatch(y -> y == 0L);
    }

    public static void main(String[] args)
    {
        LongStream.iterate(0, n -> n + 1)
                .filter(Primes::isPrime)
                .takeWhile(n -> n < 6000L)
                .forEach(System.out::println);

        LongStream.iterate(0, n -> n + 1)
                .filter(Primes::isPrime)
                .limit(783)
                .forEach(System.out::println);
    }
```

## Process streams (Intermediate operations)

### `map`

Replace each element with something else

### `filter`

Remove elements which do not match predicate


### `peek`

```java
        Arrays.stream( "AbCdEf".split(""))
                .peek(s -> System.out.println("Intermediate: " + s))
                .map(String::toUpperCase)
                .forEach(s -> System.out.println("Terminal: " + s));
```

Output:

```
Intermediate: A
Terminal: A
Intermediate: b
Terminal: B
Intermediate: C
Terminal: C
Intermediate: d
Terminal: D
Intermediate: E
Terminal: E
Intermediate: f
Terminal: F
```

### `takeWhile`

### `dropWhile`

### `limit`

### `skip`

### flatMap
```java
    var l = List.of( List.of(1, 2, 3), List.of(4, 5, 6), List.of(7, 8, 9) );
    l.stream().flatMap(Collection::stream).forEach(System.out::print); // 123456789
```

## Terminal operations

### forEach
forEach()

### Reduce

```java
        System.out.println(IntStream.rangeClosed(1, 5).reduce(1, (a, b) -> a * b) );
        System.out.println(IntStream.rangeClosed(1, 5).reduce(0, (a, b) -> a + b) );
        System.out.println(IntStream.rangeClosed(1, 5).reduce(0, Integer::sum) );

        System.out.println( IntStream.range(1, 6).reduce(Integer::min) );
        System.out.println( IntStream.range(1, 6).reduce(Integer::max) );
```

### Collect stream to collection

```java
.collect(Collectors.toList()); // ArrayList
.collect(Collectors.toUnmodifiableList()); // ImmutableCollections$ListN
.collect(Collectors.toSet()); // HashSet
.collect(Collectors.toUnmodifiableSet()); // ImmutableCollections$SetN
```

#### Collect stream to Map

##### Example 1

```java
        String[] arr = new String[] { "B", "B", "C", "A", "B", "A", "C", "B", "A" };

        var map1
            = Arrays.stream(arr)
                .sorted()
                .collect(LinkedHashMap::new,
                        (x, y) -> x.put(y, (Integer) x.getOrDefault(y, 0) + 1),
                        (a, b) -> { });

        var map2
            = Arrays.stream(arr)
                .collect(TreeMap::new,
                        (x, y) -> x.put(y, (Integer) x.getOrDefault(y, 0) + 1),
                        (a, b) -> { });
```

Output:


```
{A=3, B=4, C=2}
{A=3, B=4, C=2}
```

##### Example 2

```java
        var nameToPriceMap =
                menu.stream()
                .collect(Collectors.toMap(Dish::getName, Dish::getPrice));
```
